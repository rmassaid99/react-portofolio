import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
function Navbarku(){
    return(
        
      
      <Navbar className='navbg' variant="dark" expand="lg">
        <Container>
          <Navbar.Brand href="#home"><b>Navbar</b></Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav'/>
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className="ms-auto text-body">
                <Nav.Link href="/"><b>Home</b></Nav.Link>
                <Nav.Link href="/Skill"><b>Service</b></Nav.Link>
                <Nav.Link href="#pricing"><b>Recent Work</b></Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      
    );
}
export default Navbarku;