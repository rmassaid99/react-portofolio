function SkillItem(props){
    return(
        <div className="col-6 mt-3">
            <div className="card text dark p-3">
                <div className="card-body" id="skill">
                    <h4 className="card-title">
                        {props.judul}
                    </h4>
                    <hr/>
                    <div className="skils">
                    <img className="img" src={props.img} alt=""/>
                    <p className="card-text">
                    &ensp;&ensp;&ensp;&ensp;&ensp;{props.description}
                    </p>

                    </div>
                    <br/>
                    <a href={props.link} className="linkporto"><h5 className="card-text">
                        {props.link}
                    </h5></a>
                    
                </div>
                
            </div>
            
        </div>
    );
}
export default SkillItem;