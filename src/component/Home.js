import Dashboard from "./Dashboard";
import AboutMe from "./AboutMe";

function Home(){
    <div>
        <Dashboard/>
        <AboutMe/>
    </div>
}
export default Home;