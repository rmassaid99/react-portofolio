import Navbarku from './component/Navbarku';
import Dashboard from './component/Dashboard';
import AboutMe from './component/AboutMe';
import Services from './component/Skill';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Routes,Route } from 'react-router-dom';

function App() {
  return (

    <div>
    <Navbarku/>
      <Router>
        <Routes>
          <Route path="/" element={[<Dashboard/>,<AboutMe/>]} />
          <Route path="/skill" element={<Services/>}/>
        </Routes>
      </Router>
      
    </div>
    
    
  );
}

export default App;
